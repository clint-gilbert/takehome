Reuirements: A Java 1.8+ JVM, SBT

To run all the tests:
```
sbt test
```
Depending on your local cache, this may take a while as SBT downloads the world.

To run Step 1:
```
sbt "runMain com.example.homework.Step1 --file1 examples/pipe-delimited.txt --file2 examples/comma-delimited.txt --file3 examples/space-delimited.txt --sort-by <sort-method>"
```
where `sort-method` is one of `emailAndLastName`, `birthDate`, or `lastName`.  So for example:
```
sbt "runMain com.example.homework.Step1 --file1 examples/pipe-delimited.txt --file2 examples/comma-delimited.txt --file3 examples/space-delimited.txt --sort-by lastName"
```

To run the REST API:
```
sbt jetty:start
```
This will start a server on port 8080.  The test `com.example.homework.api.ApiTest` exercises the API with real HTTP requests.


Note: I adapted some BSD-licensed, open-source code for parsing delimited data files, hence the license and notice files.  (See `com.example.homework.Source`.)  It's overkill, but re-using it let me skip over CSV-parsing corner cases.
