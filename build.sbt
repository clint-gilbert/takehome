lazy val Versions = new {
  val LogBack = "1.2.3"
  val Scala = "2.12.12"
  val ScalaMajor = "2.12"
  val ScalaTest = "3.2.3"
  val Scallop = "3.3.0"
  val CommonsCsv = "1.7"
  val ScalaLogging = "3.9.2"
  val CommonsIO = "2.6"
  val Scalatra = "2.7.1"
  val JavaxServletApi = "3.1.0"
  val Json4s = "3.6.10"
  val Jetty = "9.4.35.v20201120"
}

lazy val mainDeps = Seq(
  "ch.qos.logback" % "logback-classic" % Versions.LogBack,
  "org.rogach" %% "scallop" % Versions.Scallop,
  "org.apache.commons" % "commons-csv" % Versions.CommonsCsv,
  "commons-io" % "commons-io" % Versions.CommonsIO,
  "com.typesafe.scala-logging" %% "scala-logging" % Versions.ScalaLogging,
  "org.scalatra" %% "scalatra" % Versions.Scalatra,
  "org.scalatra" %% "scalatra-json" % Versions.Scalatra,
  "javax.servlet" % "javax.servlet-api" % Versions.JavaxServletApi % "provided",
  "org.json4s" %% "json4s-jackson" % Versions.Json4s,
  "org.eclipse.jetty" % "jetty-webapp" % Versions.Jetty % "container"
)

lazy val testDeps = Seq(
  "org.scalatest" %% "scalatest" % Versions.ScalaTest % "test",
  "org.scalatra" %% "scalatra-scalatest" % Versions.Scalatra % "test"
)


ThisBuild / scalaVersion     := Versions.Scala
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

lazy val root = (project in file("."))
  .settings(
    name := "grtakehome",
    scalacOptions ++= Seq("-feature", "-deprecation", "-unchecked", "-Xlint", "-Xfatal-warnings"),
    libraryDependencies ++= (mainDeps ++ testDeps)
  )

enablePlugins(JettyPlugin)
