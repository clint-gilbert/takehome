package com.example.homework.api

import java.time.LocalDate

import org.apache.commons.csv.CSVFormat
import org.json4s.DefaultFormats
import org.json4s.Formats
import org.scalatra.test.scalatest.ScalatraFunSuite

import com.example.homework.Orderings
import com.example.homework.Record
import com.example.homework.Renderer

/**
 * @author clint
 * Feb 22, 2021
 */
final class ApiTest extends ScalatraFunSuite {

  //NB: API instance is shared over all test cases in this file :(
  private val api = new API()
  
  addServlet(api, "/*")

  test("POST /records - bad requests") {
    def postData(body: String): Unit = {
      post(uri = "/records", body = body.getBytes, headers = Nil) {
        assertJsonResponse(400)
      }
    }
    
    postData("")
    postData("asdf")
    postData("Smith Alice as@example.com yellow asdasdasd")
  }
  
  test("GET /records/* - no content-type specified") {
    doGetTest(None)
  }
  
  test("GET /records/* - problmatic content-type") {
    //Problematic content type for Scalatra, see https://github.com/scalatra/scalatra/issues/73
    doGetTest(Some(API.ContentTypes.urlencoded))
  }
  
  def doGetTest(postContentType: Option[String]): Unit = {
    //NB: Mutating the API's state is no fun, but it's the path of least resistance given that Scalatra makes it hard
    //to have per-test() servlet instances.  An explicity-stateful API like this isn't my preference, but it makes
    //the implementation of `State` easier.  Note that this works by accident because while test /classes/ are run 
    //concurrently by SBT, test cases within each class are run sequentially. :\
    api.clear()
    
    def assertEmptyResponse(suffix: String): Unit = {
      get(s"/records/${suffix}") {
        assertJsonResponse(200)
        
        assert(body === "[]")
      }
    }
    
    assertEmptyResponse("email")
    assertEmptyResponse("birthdate")
    assertEmptyResponse("name")
    
    val r0 = randomRecord
    val r1 = randomRecord
    val r2 = randomRecord
    
    postRecord(r0, postContentType)
    postRecord(r1, postContentType)
    postRecord(r2, postContentType)

    def assertRecordsPosted(suffix: String, ordering: Ordering[Record]): Unit = {
      get(s"/records/${suffix}") {
        assertJsonResponse(200)
        
        implicit val formats: Formats = DefaultFormats + JsonFormats.LocalDateSerializer
        
        import org.json4s._
        import org.json4s.jackson.JsonMethods._
        
        assert(parse(body).extract[List[Record]] === Seq(r0, r1, r2).sorted(ordering))
      }
    }
    
    assertRecordsPosted("email", Orderings.email)
    assertRecordsPosted("birthdate", Orderings.dateOfBirth)
    assertRecordsPosted("name", Orderings.name)
  }
  
  private def assertJsonResponse(expectedStatus: Int): Unit = {
    assert(status === expectedStatus)
    assert(header("Content-Type").startsWith("application/json"))
  }
  
  private val random = new scala.util.Random
  
  private def randomString: String = java.util.UUID.randomUUID.toString
  
  private def randomChar = "abcdefghijklmnopqrstuvwxyz".charAt(random.nextInt(26))
  
  private def randomRecord: Record = Record(
    lastName = randomString, 
    firstName = randomString,
    email = s"${randomChar}${randomChar}@example.com",
    favoriteColor = randomString,
    dateOfBirth = LocalDate.now)
  
  private def postRecord(r: Record, postContentType: Option[String]): Unit = {
    val delimiter = Seq('|', ',', ' ')(random.nextInt(3))
    
    val format = CSVFormat.DEFAULT.withDelimiter(delimiter)
    
    val renderer = Renderer(format, formatDate = _.toString)
    
    val recordLine = renderer.render(r)
    
    val contentTypeHeaders: Iterable[(String, String)] = postContentType.map(ct => ("Content-Type", ct))
    
    post(uri = "/records", body = recordLine.getBytes, headers = contentTypeHeaders) {
      assert(status === 200, body)
    }
  }
}

