package com.example.homework

import java.nio.file.Path
import org.apache.commons.io.FileUtils

/**
 * @author clint
 * Feb 19, 2021
 */
object Helpers {
  def row(headersToValues: (String, String)*): Row = new Row {
    private val map = headersToValues.toMap
    
    override def hasField(name: String): Boolean = map.contains(name)
  
    override def getFieldByName(name: String): String = map(name)
  
    override def getFieldByIndex(i: Int): String = headersToValues(i)._2
  
    override def recordNumber: Long = 1 
    
    override def size: Int = headersToValues.size
  }
  
  def withWorkDir[A](basename: String)(body: Path => A): A = {
    val workDir = java.nio.file.Files.createTempDirectory(basename)
    
    try { body(workDir) }
    finally { FileUtils.deleteQuietly(workDir.toFile) }
  }
  
  def writeTo(path: Path)(contents: String): Unit = {
    val writer = new java.io.FileWriter(path.toFile)
    
    try { writer.write(contents) }
    finally { writer.close() }
  }
  
  def withSampleDataIn[A](f: (Path, Path, Path) => A): A = {
    val f1Contents = """|LastName|FirstName|Email|FavoriteColor|DateOfBirth
                        |Doe|Jane|foo@example.com|red|2020-02-15
                        |Dob|Bad|blah@example.com|purple|ajlshfalksfh
                        |""".stripMargin.trim
                        
    val f2Contents = """|LastName,FirstName,Email,FavoriteColor,DateOfBirth
                        |Doest,Jane,foo@example.com,yellow,2020-02-16
                        |Bar,John,jb@example.com,purple,2020-02-17
                        |""".stripMargin.trim
                        
    val f3Contents = """|LastName FirstName Email FavoriteColor DateOfBirth
                        |Smith Alice as@example.com yellow 2019-02-16
                        |Jones Mike mj@example.com green dlkgfhladkg
                        |Smith Bob bs@example.com purple 2018-02-17
                        |""".stripMargin.trim
                        
    Helpers.withWorkDir(getClass.getSimpleName) { workDir =>
      val f1 = workDir.resolve("f1.txt")
      val f2 = workDir.resolve("f2.txt")
      val f3 = workDir.resolve("f3.txt")
      
      Helpers.writeTo(f1)(f1Contents)
      Helpers.writeTo(f2)(f2Contents)
      Helpers.writeTo(f3)(f3Contents)
      
      f(f1, f2, f3)
    }
  }
}
