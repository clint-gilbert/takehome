package com.example.homework

import org.scalatest.funsuite.AnyFunSuite
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Buffer

/**
 * @author clint
 * Feb 21, 2021
 */
final class RecordsTest extends AnyFunSuite {
  test("from") {
    Helpers.withSampleDataIn { (f1, f2, f3) =>
      
      val errors: Buffer[String] = new ArrayBuffer
      
      val source = Records.from(f1, f2, f3, errors += _)
      
      assert(errors.isEmpty)
      
      val records = source.records.toSet
      
      assert(errors.size === 2)
      
      val expectedNames = Set("Jane Doe",
                              "Jane Doest",
                              "John Bar",
                              "Alice Smith",
                              "Bob Smith")
                         
      assert(records.map(r => s"${r.firstName} ${r.lastName}") === expectedNames)
    }
  }
}
