package com.example.homework

import org.scalatest.funsuite.AnyFunSuite

/**
 * @author clint
 * Feb 21, 2021
 */
final class IntentTest extends AnyFunSuite {
  private def cliConf(s: String): CliConfig = CliConfig(s.split("\\s+"))
  
  import java.nio.file.Paths.{get => path}
  
  test("files") {
    val conf = cliConf("--file1 foo.txt --file2 bar.txt --file3 baz.txt --sort-by lastName")
    
    val expected = Right((path("foo.txt"), path("bar.txt"), path("baz.txt")))
    
    assert(conf.inputFiles === expected)
  }
  
  test("sort method") {
    {
      val conf = cliConf("--file1 a --file2 b --file3 c --sort-by lastName")
      
      assert(conf.toIntent === Right(Intent.SortByLastNameDesc))
    }
    
    {
      val conf = cliConf("--file1 a --file2 b --file3 c --sort-by birthdate")
      
      assert(conf.toIntent === Right(Intent.SortByDateOfBirthAsc))
    }
    
    {
      val conf = cliConf("--file1 a --file2 b --file3 c --sort-by emailAndLastName")
      
      assert(conf.toIntent === Right(Intent.SortByEmailDescLastNameAsc))
    }
  }
}
