package com.example.homework

import org.scalatest.funsuite.AnyFunSuite

/**
 * @author clint
 * Feb 19, 2021
 */
final class RowTest extends AnyFunSuite {
  test("CommonsCsvRow") {
    val csvData = {
"""
X Y Z
a 1 t
b 2 u
c 3 v
""".trim
    }
  
    val source = Source.fromString(csvData, Source.Formats.spaceDelimitedWithHeader)
    
    val Seq(r1, r2, r3) = source.records.toList
    
    def doTest(r: Row)(expectedX: String, expectedY: String, expectedZ: String): Unit = {
      assert(r.size === 3)
      
      assert(r.toList === List(expectedX, expectedY, expectedZ)) //tests `iterator`
      
      assert(r.hasField("X"))
      assert(r.hasField("Y"))
      assert(r.hasField("Z"))
      assert(!r.hasField("q"))
      assert(!r.hasField(""))
      
      assert(r.getFieldByName("X") === expectedX)
      assert(r.getFieldByName("Y") === expectedY)
      assert(r.getFieldByName("Z") === expectedZ)
  
      assert(r.getFieldByIndex(0) === expectedX)
      assert(r.getFieldByIndex(1) === expectedY)
      assert(r.getFieldByIndex(2) === expectedZ)
    }
    
    doTest(r1)("a", "1", "t")
    doTest(r2)("b", "2", "u")
    doTest(r3)("c", "3", "v")
  }
  
  
}
