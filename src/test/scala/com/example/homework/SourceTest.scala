package com.example.homework

import java.io.StringReader
import java.nio.file.Path

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Buffer

import org.scalatest.funsuite.AnyFunSuite

/**
 * @author clint
 * Feb 19, 2021
 */
final class SourceTest extends AnyFunSuite {
  test("map") {
    val is = Source.fromIterable(Seq(1,2,3)).map(_ + 1)
    
    val expected = Seq(2, 3, 4)
    
    assert(is.records.toList === expected)
  }
  
  test("flatMap") {
    val is = Source.fromIterable(Seq(1,2,3)).flatMap { i => Source.fromIterable(Seq.fill(i)(i * 2)) }
    
    val expected = Seq(2, 4, 4, 6, 6, 6)
    
    assert(is.records.toList === expected)
  }

  test("take") {
    val is = Source.fromIterable(Seq(1,2,3,4,5)).take(3)
    
    assert(is.records.toList === Seq(1,2,3))
  }
  
  test("drop") {
    val is = Source.fromIterable(Seq(1,2,3,4,5)).drop(3)
    
    assert(is.records.toList === Seq(4,5))
  }
  
  test("collect") {
    val is = Source.fromIterable(Seq(1,2,3,4,5)).collect { case i if i % 2 != 0 => i.toString }
    
    assert(is.records.toList === Seq("1", "3", "5"))
  }
  
  test("foreach") {
    val buffer: Buffer[Int] = new ArrayBuffer
    
    val is = Source.fromIterable(Seq(1,2,3,4,5))
    
    is.foreach(buffer += _)
    
    assert(buffer.toSeq === Seq(1,2,3,4,5))
  }
  
  test("++") {
    val is = Source.fromIterable(Seq(1)) ++
             Source.fromIterable(Seq(2,3)) ++
             Source.fromIterable(Seq(4,5))
             
    assert(is.records.toList === Seq(1,2,3,4,5))
  }
  
  private val csvData = {
"""
X Y Z
a 1 t
b 2 u
c 3 v
""".trim
  }
  
  private def doFromTest(s: Source[Row]): Unit = {
    val expected = Seq(
      "a 1 t",
      "b 2 u",
      "c 3 v")
      
    assert(s.records.map(_.mkString(" ")).toList === expected)
  }
  
  test("fromString") {
    doFromTest(Source.fromString(csvData, Source.Formats.spaceDelimitedWithHeader))
  }
  
  private def writeTo(file: Path)(contents: String): Unit = {
    val writer = new java.io.FileWriter(file.toFile)
      
    try { writer.write(contents) } finally { writer.close() }
  }
  
  test("fromFile") {
    Helpers.withWorkDir(getClass.getSimpleName) { workDir =>
      val file = workDir.resolve("data.csv")
      
      writeTo(file)(csvData)      
      
      doFromTest(Source.fromFile(file))
    }
  }
  
  test("fromReader") {
    doFromTest(Source.fromReader(new StringReader(csvData)))
  }
}
