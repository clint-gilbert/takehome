package com.example.homework

import java.time.LocalDate

import org.scalatest.funsuite.AnyFunSuite

import Record.DefaultColumnNames

/**
 * @author clint
 * Feb 19, 2021
 */
final class RecordTest extends AnyFunSuite {
  private val dob = LocalDate.parse("2019-02-15")
  
  test("values") {
    val record = Record(
      lastName = "Doe", 
      firstName = "John",
      email = "foo@example.com",
      favoriteColor = "purple",
      dateOfBirth = dob)
    
    val expected = Seq(
        "Doe",
        "John",
        "foo@example.com",
        "purple",
        "02/15/2019")
    
    assert(record.values(Record.formatter.format) === expected)
  }
  
  test("fromRow - happy path") {
    //Column order shouldn't matter
    val row = Helpers.row(
        DefaultColumnNames.dateOfBirth -> dob.toString,
        DefaultColumnNames.firstName -> "Jane",
        DefaultColumnNames.email -> "blarg@example.com",
        DefaultColumnNames.lastName -> "Doe",
        DefaultColumnNames.favoriteColor -> "red")
        
    val expected = Right(Record(
        lastName = "Doe", 
        firstName = "Jane",
        email = "blarg@example.com",
        favoriteColor = "red",
        dateOfBirth = dob))
        
    assert(Record.fromRow(row) === expected)
  }
  
  test("fromRow - columns fail to parse") {
    def doTest(badColumn: String): Unit = {
      val headersToValues = Seq(
        DefaultColumnNames.dateOfBirth -> dob.toString,
        DefaultColumnNames.firstName -> "Jane",
        DefaultColumnNames.email -> "blarg@example.com",
        DefaultColumnNames.lastName -> "Doe",
        DefaultColumnNames.favoriteColor -> "red")  
      
      val withoutBadColumn = headersToValues.filter { case (h, v) => h != badColumn }
        
      val row = Helpers.row(withoutBadColumn: _*)
        
      assert(Record.fromRow(row).isLeft)
    }
    
    doTest(DefaultColumnNames.dateOfBirth)
    doTest(DefaultColumnNames.firstName)
    doTest(DefaultColumnNames.email)
    doTest(DefaultColumnNames.lastName)
    doTest(DefaultColumnNames.favoriteColor)
  }
}
