package com.example.homework

import org.rogach.scallop.ScallopConf
import org.rogach.scallop.ScallopOption
import java.nio.file.Path
import CliConfig.SortMethods

/**
 * @author clint
 * Feb 19, 2021
 */
final case class CliConfig (private val arguments: Seq[String]) extends ScallopConf(arguments) {
  val file1: ScallopOption[Path] = opt[Path](descr = "Pipe-delimited file")
  val file2: ScallopOption[Path] = opt[Path](descr = "Comma-delimited file")
  val file3: ScallopOption[Path] = opt[Path](descr = "Space-delimited file")
  
  val sortBy: ScallopOption[String] = opt[String](
      descr = s"Field(s) to sort by.  Allowed values are: " +
              s"${SortMethods.EmailAndLastName.name}, ${SortMethods.BirthDate.name}, ${SortMethods.LastName.name}")

  //Required by Scallop
  verify()
              
  def toIntent: Either[String, Intent] = sortBy.toOption.map(_.toLowerCase) match {
    case Some(SortMethods.EmailAndLastName()) => Right(Intent.SortByEmailDescLastNameAsc)
    case Some(SortMethods.BirthDate()) => Right(Intent.SortByDateOfBirthAsc)
    case Some(SortMethods.LastName()) => Right(Intent.SortByLastNameDesc)
    case Some(other) => {
      printHelp()
      
      Left(s"Unknown --sort-by param '$other'")
    }
    case None => {
      printHelp()
      
      Left(s"--sort-by param must be supplied")
    }
  }
  
  def inputFiles: Either[String, (Path, Path, Path)] = {
    if(file1.isEmpty) { Left("--file1 must be supplied") }
    else if(file2.isEmpty) { Left("--file2 must be supplied") }
    else if(file3.isEmpty) { Left("--file3 must be supplied") }
    else { Right((file1(), file2(), file3())) }
  }
}

object CliConfig {
  object SortMethods {
    object EmailAndLastName extends Matcher("emailAndLastName")
    object BirthDate extends Matcher("birthDate")
    object LastName extends Matcher("lastName")
    
    abstract class Matcher(val name: String) {
      def unapply(s: String): Boolean = s.toLowerCase == name.toLowerCase
    }
  }
  
  
}
