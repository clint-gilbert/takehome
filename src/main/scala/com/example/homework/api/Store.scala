package com.example.homework.api

import com.example.homework.Record

/**
 * @author clint
 * Feb 21, 2021
 */
final class Store {
  @volatile private[this] var _records: Seq[Record] = Nil
  private[this] val lock = new AnyRef
  
  def clear(): Unit = lock.synchronized { _records = Nil }
  
  def records: Seq[Record] = lock.synchronized { _records }
  
  def ++=(rs: TraversableOnce[Record]): Unit = lock.synchronized {
    _records ++= rs.toList 
  }
}
