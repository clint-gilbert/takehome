package com.example.homework.api

import org.json4s.CustomSerializer
import java.time.LocalDate
import org.json4s.JsonAST.JValue
import org.json4s.JsonAST.JString

/**
 * @author clint
 * Feb 22, 2021
 */
object JsonFormats {
  private val deserializeLocalDate: PartialFunction[JValue, LocalDate] = {
    case JString(s) => LocalDate.parse(s)
  }
  
  private val serializeLocalDate: PartialFunction[Any, JValue] = {
    case ld: LocalDate => JString(ld.toString)
  }
  
  case object LocalDateSerializer extends CustomSerializer[LocalDate]( format =>
    (deserializeLocalDate, serializeLocalDate)
  )
}
