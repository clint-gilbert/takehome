package com.example.homework.api

import org.apache.commons.csv.CSVFormat
import org.json4s.DefaultFormats
import org.json4s.Formats
import org.scalatra.BadRequest
import org.scalatra.Ok
import org.scalatra.ScalatraServlet
// JSON handling support from Scalatra
import org.scalatra.json.JacksonJsonSupport

import com.example.homework.Orderings
import com.example.homework.Record
import com.example.homework.Source

/**
 * @author clint
 * Feb 21, 2021
 *
 */
final class API(store: Store = new Store) extends ScalatraServlet with JacksonJsonSupport {

  override protected implicit val jsonFormats: Formats = DefaultFormats + JsonFormats.LocalDateSerializer
  
  private[api] def clear(): Unit = store.clear()
  
  before() {
    contentType = API.ContentTypes.json
  }
  
  post("/records") {
    val body = bodyAsString()
    
    val recordSource = parseRecordLines(body)
    
    val records = recordSource.records.toList
    
    val successes = records.collect { case Right(r) => r }
    
    val failureMessages = records.collect { case Left(msg) => msg }
    
    if(successes.isEmpty) {
      BadRequest(failureMessages.headOption.getOrElse(""))
    } else {
      store ++= successes
    
      Ok()
    }
  }
  
  get("/records/email") {
    store.records.sorted(Orderings.email)
  }
  
  get("/records/birthdate") {
    store.records.sorted(Orderings.dateOfBirth)
  }
  
  get("/records/name") {
    store.records.sorted(Orderings.name)
  }
  
  /*
   * Adaptation of a hacky workaround presented for this issue: 
   * 
   * https://github.com/scalatra/scalatra/issues/73
   * 
   * Basically, request.body is the empty string if a request's content-type is 
   * "application/x-www-form-urlencoded", which is what curl supplies, at least for POST requests, 
   * if no content-type is explicitly specified.
   */
  private def bodyAsString(): String = {
    val contentLength = request.contentLength.getOrElse(-1L).toInt
    
    val contentType = request.contentType.getOrElse("") 
    
    if(contentType == API.ContentTypes.urlencoded) {
      if (contentLength < 1) { "" }
      else {
        val itr = params.iterator
        
        if(itr.hasNext) itr.next()._1 else ""
      }
    } else {
      request.body
    }
  }
  
  private def parseRecordLines(postBody: String): Source[Either[String,Record]] = {
    def sourceFor(delimiter: Char): Source[Either[String, Record]] = {
      val format = CSVFormat.DEFAULT
        .withDelimiter(delimiter)
        .withHeader(Record.DefaultColumnNames.asHeaders: _*)
        
      Source.fromString(postBody, format).map(Record.fromRow)
    }
    
    Seq('|', ',', ' ').map(sourceFor).reduce(_ ++ _)
  }
}

object API {
  object ContentTypes {
    val json: String = "application/json"
    val urlencoded = "application/x-www-form-urlencoded" 
  }  
}
