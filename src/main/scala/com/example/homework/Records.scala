package com.example.homework

import java.nio.file.Path
import com.typesafe.scalalogging.LazyLogging

/**
 * @author clint
 * Feb 21, 2021
 */
object Records extends LazyLogging {
  def from(
      pipeDelimited: Path,
      commaDelimited: Path,
      spaceDelimited: Path,
      log: String => Unit = defaultLogFn): Source[Record] = {
    
    val source = {
      sourceFor(pipeDelimited, delimiter = '|') ++
      sourceFor(commaDelimited, delimiter = ',') ++
      sourceFor(spaceDelimited, delimiter = ' ')
    }
    
    logFailuresAndKeepSuccesses(source, log)
  }
  
  def logFailuresAndKeepSuccesses(
      source: Source[Either[String, Record]], 
      log: String => Unit = defaultLogFn): Source[Record] = source.map(logFailures(log)).collect(keepSuccesses)

  private val defaultLogFn: String => Unit = logger.warn(_)
      
  private def logFailures[A](log: String => Unit)(e: Either[String, A]): Either[String, A] = e match {
    case l @ Left(msg) => {
      log(msg)
      
      l
    }
    case r => r
  }
  
  private def keepSuccesses[A]: PartialFunction[Either[String, A], A] = {
    case Right(a) => a
  }
  
  private def sourceFor(file: Path, delimiter: Char): Source[Either[String, Record]] = {
    val format = Source.Defaults.csvFormat.withDelimiter(delimiter)
    
    Source.fromFile(file, format).map(Record.fromRow)
  }
}
