package com.example.homework

import org.apache.commons.csv.CSVFormat
import java.time.LocalDate

/**
 * @author clint
 * Feb 21, 2021
 */
final case class Renderer(format: CSVFormat, formatDate: LocalDate => String = Record.formatter.format) {
  def render(r: Record): String = format.format(r.values(formatDate): _*)
}
