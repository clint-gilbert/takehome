package com.example.homework


/**
 * @author clint
 * Feb 21, 2021
 */
object Orderings {
  val email: Ordering[Record] = Ordering.by(_.email)
  //Simplifying assumption: use system time zone in all cases; 
  //obviously a bad idea in a real system that deals with times.
  val dateOfBirth: Ordering[Record] = Ordering.fromLessThan[Record] { (a, b) => 
    a.dateOfBirth.compareTo(b.dateOfBirth) < 0 
  }

  val lastName: Ordering[Record] = Ordering.by(_.lastName)
  val firstName: Ordering[Record] = Ordering.by(_.firstName)
  
  import Implicits.OrderingOps
  
  val name: Ordering[Record] = lastName orElse firstName
  
  object Implicits {
    final implicit class OrderingOps[A](val lhs: Ordering[A]) extends AnyVal {
      def orElse(rhs: Ordering[A]): Ordering[A] = Ordering.comparatorToOrdering {
        lhs.thenComparing(rhs)
      } 
    }
  }
}
