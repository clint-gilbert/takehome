package com.example.homework

import java.io.FileReader
import java.io.Reader
import java.io.StringReader
import java.nio.file.Path
import java.nio.file.Paths

import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser

import com.example.util.TakesEndingActionIterator
import com.example.util.Throwables
import com.typesafe.scalalogging.LazyLogging

/**
 * @author clint
 * Dec 17, 2019
 */
sealed trait Source[R] {
  def ++(rs: Source[R]): Source[R] = Source.fromIterator(records ++ rs.records)
  
  def records: Iterator[R]
  
  def take(n: Int): Source[R] = fromCombinator(_.take(n))
  
  def drop(n: Int): Source[R] = fromCombinator(_.drop(n))
  
  //NB: Lots more combinators possible, at least everything from Iterator[R]
  
  private final def fromCombinator[S](f: Iterator[R] => Iterator[S]): Source[S] = {
    new Source.FromIterator(f(records))
  }
  
  def map[S](f: R => S): Source[S] = fromCombinator(_.map(f))
  
  def flatMap[S](f: R => Source[S]): Source[S] = fromCombinator(_.flatMap(f(_).records))

  def collect[S](pf: PartialFunction[R, S]): Source[S] = fromCombinator(_.collect(pf))
  
  def foreach[A](f: R => A): Unit = records.foreach(f)
}
  
object Source extends LazyLogging {
  
  def producing[A](a: => A): Source[A] = Source.fromIterable(Iterable(a))
  
  object Formats {
    val spaceDelimited: CSVFormat = CSVFormat.DEFAULT.withDelimiter(' ')
    
    val spaceDelimitedWithHeader: CSVFormat = spaceDelimited.withFirstRecordAsHeader
      
    val tabDelimited: CSVFormat = CSVFormat.DEFAULT.withDelimiter('\t')
    
    val tabDelimitedWithHeader: CSVFormat = tabDelimited.withFirstRecordAsHeader
  }
  
  object Defaults {
    val csvFormat: CSVFormat = Formats.tabDelimitedWithHeader 
    
    val thisDir: Path = Paths.get(".")
  }
  
  private final class FromIterable[R](rs: => Iterable[R]) extends Source[R] {
    override def records: Iterator[R] = rs.iterator
  }
  
  private object FromIterable {
    def apply[R](rs: => Iterable[R]): FromIterable[R] = new FromIterable(rs)
  }
  
  private final class FromIterator[R](rs: => Iterator[R]) extends Source[R] {
    override def records: Iterator[R] = rs
  }
  
  private object FromIterator {
    def apply[R](rs: => Iterator[R]): FromIterator[R] = new FromIterator(rs)
  }
  
  def fromIterable[R](rs: Iterable[R]): Source[R] = FromIterable(rs)
   
  private def fromIterator[R](rs: => Iterator[R]): Source[R] = FromIterator(rs)
  
  def fromFile(
      path: Path, 
      csvFormat: CSVFormat = Defaults.csvFormat): Source[Row] = {
    
    fromReader(new FileReader(path.toFile), csvFormat)
  }
  
  def fromString(
      csvData: String,
      csvFormat: CSVFormat = Defaults.csvFormat): Source[Row] = {
    
    fromReader(new StringReader(csvData), csvFormat)
  }
  
  def fromReader(
      reader: => Reader, 
      csvFormat: CSVFormat = Defaults.csvFormat): Source[Row] = {
    
    FromIterator(toCsvRowIterator(reader, csvFormat))
  }

  private def toCsvRowIterator(reader: Reader, csvFormat: CSVFormat): Iterator[Row] = {
    import scala.collection.JavaConverters._
      
    val parser = new CSVParser(reader, csvFormat)
    
    val iterator = parser.iterator.asScala.map(Row.CommonsCsvRow(_))
    
    TakesEndingActionIterator(iterator) {
      Throwables.quietly("Closing CSV parser", logger.error(_, _))(parser.close())
      Throwables.quietly("Closing underlying reader", logger.error(_, _))(reader.close())
    }
  }
}
