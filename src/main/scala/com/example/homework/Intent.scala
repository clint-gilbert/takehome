package com.example.homework

/**
 * @author clint
 * Feb 19, 2021
 */
sealed trait Intent {
  def ordering: Ordering[Record] 
}

object Intent {
  case object SortByEmailDescLastNameAsc extends Intent {
    import Orderings.Implicits._ //get `orElse`
    
    override def ordering: Ordering[Record] = Orderings.email.reverse orElse Orderings.lastName
  }
  
  case object SortByDateOfBirthAsc extends Intent {
    override def ordering: Ordering[Record] = Orderings.dateOfBirth
  }
  
  case object SortByLastNameDesc extends Intent {
    override def ordering: Ordering[Record] = Orderings.lastName.reverse
  }
}
