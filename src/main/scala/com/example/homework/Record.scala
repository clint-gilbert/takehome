package com.example.homework

import java.time.LocalDate

import scala.util.control.NonFatal
import java.time.format.DateTimeFormatter
import scala.util.Try

/**
 * @author clint
 * Feb 19, 2021
 */
final case class Record(
    lastName: String, 
    firstName: String,
    email: String,
    favoriteColor: String,
    dateOfBirth: LocalDate) {

  def values(implicit formatDate: LocalDate => String): Seq[String] = Seq(
    lastName,
    firstName,
    email,
    favoriteColor,
    formatDate(dateOfBirth)
  )
}

object Record {
  def fromRow(row: Row): Either[String, Record] = {
    def column(name: String): Either[String, String] = {
      if(row.hasField(name)) { Try(row.getFieldByName(name)).toEither.left.map(_.getMessage) }
      else { Left(s"Column '$name' now found in $row") }
    }
    
    for {
      lastName <- column(DefaultColumnNames.lastName) 
      firstName <- column(DefaultColumnNames.firstName)
      email <- column(DefaultColumnNames.email)
      favoriteColor <- column(DefaultColumnNames.favoriteColor)
      dobString <- column(DefaultColumnNames.dateOfBirth)
      dateOfBirth <- parseDob(dobString)
    } yield {
      Record(
        lastName = lastName,
        firstName = firstName,
        email = email,
        favoriteColor = favoriteColor,
        dateOfBirth = dateOfBirth)
    }
  }
  
  private[homework] def parseDob(s: String): Either[String, LocalDate] = {
    try {
      Right(LocalDate.parse(s))
    } catch {
      case NonFatal(e) => Left(s"Couldn't parse '$s' as a LocalDateTime: ${e.getMessage}")
    }
  }
  
  val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/YYYY")
 
  object DefaultColumnNames {
    val lastName: String = "LastName" 
    val firstName: String = "FirstName"
    val email: String = "Email"
    val favoriteColor: String = "FavoriteColor"
    val dateOfBirth: String = "DateOfBirth" 
    
    val asHeaders: Seq[String] = Seq(lastName, firstName, email, favoriteColor, dateOfBirth)
  }
}
