package com.example.homework

import org.apache.commons.csv.CSVRecord

/**
 * @author clint
 * Feb 10, 2020
 */
trait Row extends Iterable[String] {
  def hasField(name: String): Boolean
  
  def getFieldByName(name: String): String
  
  def getFieldByIndex(i: Int): String
  
  def recordNumber: Long 
  
  override def iterator: Iterator[String] = (0 until size).iterator.map(getFieldByIndex)
}

object Row {
  final case class CommonsCsvRow(delegate: CSVRecord) extends Row {
    
    override def hasField(name: String): Boolean = delegate.isMapped(name)
    
    override def getFieldByName(name: String): String = delegate.get(name)
    
    override def getFieldByIndex(i: Int): String = delegate.get(i)
    
    override def size: Int = delegate.size
    
    override def recordNumber: Long = delegate.getRecordNumber
  }
}
