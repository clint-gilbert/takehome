package com.example.util

import scala.util.control.NonFatal

/**
 * @author clint
 * Dec 7, 2016
 */
object Throwables {
  def quietly(
      message: String, 
      doLog: (String, Throwable) => Any)
      (f: => Any): Option[Throwable] = {
    
    try { 
      f 
      
      None
    } catch { 
      case NonFatal(e) => {
        doLog(message, e)
        
        Some(e)
      }
    }
  }
}
