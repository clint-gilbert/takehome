package com.example.util

/**
 * @author clint
 * date: Aug 8, 2016
 * 
 * An Iterator that takes some action - invokes the passed-in onFinished block - when it is exhausted, and 
 * delegates to another Iterator for everything else.
 */
final class TakesEndingActionIterator[A](delegate: Iterator[A])(onFinished: => Any) extends Iterator[A] {
  @volatile private[this] var invokedOnFinish: Boolean = false
  private[this] val lock = new AnyRef
  
  override def hasNext: Boolean = delegate.hasNext

  override def next(): A = {
    try { delegate.next() }
    finally {
      lock.synchronized { 
        if(!hasNext && !invokedOnFinish) {
          onFinished
          
          invokedOnFinish = true
        }
      }
    }
  }
}

object TakesEndingActionIterator {
  def apply[A](delegate: Iterator[A])(onFinished: => Any): TakesEndingActionIterator[A] = {
    new TakesEndingActionIterator(delegate)(onFinished)
  }
}
