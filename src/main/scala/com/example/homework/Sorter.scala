package com.example.homework

/**
 * @author clint
 * Feb 19, 2021
 */
final case class Sorter(intent: Intent) {
  def sort(source: Source[Record]): Seq[Record] = {
      source
        .records
        .toSeq
        .sorted(intent.ordering)
  }
}
