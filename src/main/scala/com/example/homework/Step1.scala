package com.example.homework

import com.typesafe.scalalogging.LazyLogging

/**
 * @author clint
 * Feb 19, 2021
 */
final object Step1 extends LazyLogging {
  def main(args: Array[String]): Unit = {
    val conf = new CliConfig(args)
    
    val resultE = for {
      intent <- conf.toIntent
      files <- conf.inputFiles
    } yield {
      val (f1, f2, f3) = files
      
      val source = Records.from(f1, f2, f3)
      
      val sortedRecords = Sorter(intent).sort(source)
      
      //NB: Chosen arbitrarily; could easily come from config
      val outputFormat = Source.Formats.spaceDelimited
      
      val renderer = Renderer(outputFormat)
      
      sortedRecords.foreach(r => println(renderer.render(r)))
    }
    
    for {
      msg <- resultE.left
    } {
      logger.error(msg)
    }
  }
}
