//NB: This must be present in the default package

import org.scalatra._
import javax.servlet.ServletContext
import com.example.homework.api.API

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {
    context.mount(new API(), "/*")
  }
}
